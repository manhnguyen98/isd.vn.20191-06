package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import constant.Constant;
import main.Setup;
import view.UserInterface;

public class PrepaidCard extends TravelPermit {

	private float balance;

	private int embarkationId;

	public PrepaidCard(String cardId, float baseFare, String lastAction, float balance, int embarkationId) {
		super(cardId, baseFare, lastAction);
		this.balance = balance;
		this.embarkationId = embarkationId;
	}

	/**
	 * Get a prepaid card balance specified by ID from database
	 * 
	 * @param id the prepaid card ID
	 * @return the prepaid card balance
	 * @throws SQLException if there was a problem querying the database
	 */
	public static float getBalanceById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT balance FROM prepaid_cards WHERE id = \"" + id + "\"");
		rs.next();
		float balance = rs.getFloat("balance");
		Setup.closeConnectionToDatabase(conn);
		return balance;
	}

	/**
	 * Get the station ID that the passenger embarked using the prepaid card
	 * specified by ID from database
	 * 
	 * @param id the prepaid card ID
	 * @return the embarkation ID of passenger
	 * @throws SQLException if there was a problem querying the database
	 */
	public static int getEmbarkationIdById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT embarkation_id FROM prepaid_cards WHERE id = \"" + id + "\"");
		rs.next();
		int embarkationId = rs.getInt("embarkation_id");
		Setup.closeConnectionToDatabase(conn);
		return embarkationId;
	}

	/**
	 * Save this prepaid card information to database
	 * 
	 * @see model.TravelPermit#saveToDb()
	 */
	@Override
	protected void saveToDb() throws SQLException {
		Connection conn = Setup.connectToDatabase();
		Setup.executeUpdate(conn,
				"UPDATE travel_permits SET last_action = \"" + this.lastAction + "\" WHERE id = \"" + this.id + "\"");
		Setup.executeUpdate(conn, "UPDATE prepaid_cards SET balance = " + this.balance + ", embarkation_id = "
				+ this.embarkationId + " WHERE id = \"" + this.id + "\"");
		Setup.closeConnectionToDatabase(conn);
	}

	/**
	 * @param embarkationId the embarkation ID of passenger
	 * @return true if the passenger can enter the platform area at the station
	 *         specified by the given ID with this prepaid card, false otherwise
	 * @see model.TravelPermit#enterStation(int)
	 */
	@Override
	public boolean enterStation(int embarkationId) throws SQLException {
		if (this.lastAction != null)
			if (this.lastAction.equals("enter")) {
				UserInterface.setMessage("The card is already used to enter a station before");
				return false;
			}

		if (this.balance < Constant.BASE_FARE) {
			UserInterface.setMessage("Not enough balance: Expected " + Constant.BASE_FARE + " " + Constant.CURRENCY);
			return false;
		}

		this.lastAction = "enter";
		this.embarkationId = embarkationId;
		this.saveToDb();
		return true;
	}

	/**
	 * @param disembarkationId the disembarkation ID of passenger
	 * @return true if the passenger can exit the platform area at the station
	 *         specified by the given ID with this prepaid card, false otherwise
	 * @see model.TravelPermit#exitStation(int)
	 */
	@Override
	public boolean exitStation(int disembarkationId) throws SQLException {
		if (this.lastAction == null || this.lastAction.equals("exit")) {
			UserInterface.setMessage("The card is not used to enter a station before");
			return false;
		}

		float fare = StationBasedFareCalculation.calculateFare(this.embarkationId, disembarkationId);

		if (this.balance < fare) {
			UserInterface.setMessage("Not enough balance: Expected " + fare + " " + Constant.CURRENCY);
			return false;
		}

		this.lastAction = "exit";
		this.balance -= fare;
		this.saveToDb();
		return true;
	}

}
