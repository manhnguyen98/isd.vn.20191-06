package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import main.Setup;
import view.UserInterface;

public class TwentyFourHourTicket extends TravelPermit {

	private Timestamp firstUsedTime;

	public TwentyFourHourTicket(String ticketId, float baseFare, String lastAction, Timestamp firstUsedTime) {
		super(ticketId, baseFare, lastAction);
		this.firstUsedTime = firstUsedTime;
	}

	/**
	 * Get the time when a 24-hour ticket specified by ID is first used from
	 * database
	 * 
	 * @param id the 24-hour ticket ID
	 * @return the 24-hour ticket first used time
	 * @throws SQLException if there was a problem querying the database
	 */
	public static Timestamp getFirstUsedTimeById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn,
				"SELECT first_used_time FROM 24_hour_tickets WHERE id = \"" + id + "\"");
		rs.next();
		Timestamp firstUsedTime = rs.getTimestamp("first_used_time");
		Setup.closeConnectionToDatabase(conn);
		return firstUsedTime;
	}

	/**
	 * Save this 24-hour ticket information to database
	 * 
	 * @see model.TravelPermit#saveToDb()
	 */
	@Override
	protected void saveToDb() throws SQLException {
		Connection conn = Setup.connectToDatabase();
		Setup.executeUpdate(conn,
				"UPDATE travel_permits SET last_action = \"" + this.lastAction + "\" WHERE id = \"" + this.id + "\"");
		Setup.executeUpdate(conn, "UPDATE 24_hour_tickets SET first_used_time = \"" + this.firstUsedTime.toString()
				+ "\" WHERE id = \"" + this.id + "\"");
		Setup.closeConnectionToDatabase(conn);
	}

	/**
	 * @param embarkationId the embarkation ID of passenger
	 * @return true if the passenger can enter the platform area at the station
	 *         specified by the given ID with this 24-hour ticket, false otherwise
	 * @see model.TravelPermit#enterStation(int)
	 */
	@Override
	public boolean enterStation(int embarkationId) throws SQLException {
		if (this.lastAction != null)
			if (this.lastAction.equals("enter")) {
				UserInterface.setMessage("The ticket is already used to enter a station before");
				return false;
			}

		Timestamp current = new Timestamp(System.currentTimeMillis());

		if (this.firstUsedTime == null) {
			this.firstUsedTime = current;
			this.lastAction = "enter";
			this.saveToDb();
			return true;
		}

		if (current.getTime() < this.firstUsedTime.getTime() + 24 * 3600 * 1000) {
			this.lastAction = "enter";
			saveToDb();
			return true;
		}

		UserInterface.setMessage("Expired: Tried to enter at " + new Date(current.getTime()));
		return false;
	}

	/**
	 * @param disembarkationId the disembarkation ID of passenger
	 * @return true if the passenger can exit the platform area at the station
	 *         specified by the given ID with this 24-hour ticket, false otherwise
	 * @see model.TravelPermit#exitStation(int)
	 */
	@Override
	public boolean exitStation(int disembarkationId) throws SQLException {
		if (this.lastAction == null || this.lastAction.equals("exit")) {
			UserInterface.setMessage("The ticket is not used to enter a station before");
			return false;
		}

		this.lastAction = "exit";
		this.saveToDb();
		return true;
	}

}
