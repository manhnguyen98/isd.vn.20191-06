package view;

import main.Setup;

public class GateControlInterface {

	private static GateControlInterface instance = new GateControlInterface();

	/**
	 * @return the instance of class GateControlInterface
	 */
	public static GateControlInterface getInstance() {
		return instance;
	}

	private GateControlInterface() {

	}

	/**
	 * Send signal to open gate
	 * 
	 * @see hust.soict.se.gate.Gate#open()
	 */
	public static void openGate() {
		Setup.gate.open();
	}

	/**
	 * Send signal to close gate
	 * 
	 * @see hust.soict.se.gate.Gate#close()
	 */
	public static void closeGate() {
		Setup.gate.close();
	}

}
