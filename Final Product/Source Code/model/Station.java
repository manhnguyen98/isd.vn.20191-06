package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import main.Setup;

public class Station {

	/**
	 * @param stationId the ID of station
	 * @return the name of station
	 * @throws SQLException
	 */
	public static String getNameById(int stationId) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT name FROM stations WHERE id = " + stationId);
		rs.next();
		String name = rs.getString("name");
		Setup.closeConnectionToDatabase(conn);
		return name;
	}

	/**
	 * @param stationId the ID of station
	 * @return the distance of station
	 * @throws SQLException
	 */
	public static float getDistanceById(int stationId) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT distance FROM stations WHERE id = " + stationId);
		rs.next();
		float distance = rs.getFloat("distance");
		Setup.closeConnectionToDatabase(conn);
		return distance;
	}

}
