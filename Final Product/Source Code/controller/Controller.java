package controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import constant.Constant;
import hust.soict.se.customexception.InvalidIDException;
import main.Setup;
import model.OneWayTicket;
import model.PrepaidCard;
import model.TravelPermit;
import model.TwentyFourHourTicket;
import view.GateControlInterface;
import view.UserInterface;

public class Controller {

    private static Controller instance = new Controller();

    /**
     * @return the instance of class Controller
     */
    public static Controller getInstance() {
        return instance;
    }

    private Controller() {

    }

    /**
     * @param barcode the barcode string
     * @return the code of ticket/card
     * @throws InvalidIDException when the barcode is invalid
     */
    private String scanBarcode(String barcode) throws InvalidIDException {
        if (Character.isUpperCase(barcode.charAt(0)))
            return Setup.cardScanner.process(barcode);
        else
            return Setup.ticketRecognizer.process(barcode);
    }

    /**
     * @param code the code of ticket/card
     * @return the creation of Travel Permit Instance
     * @throws SQLException
     */
    private TravelPermit createTravelPermitInstance(String code) throws SQLException {
        String id = TravelPermit.getIdByCode(code);

        UserInterface.setTravelPermitId(id);

        if (id.startsWith(Constant.ONE_WAY_TICKET_PREFIX))
            return createOneWayTicketInstance(id);
        else if (id.startsWith(Constant.TWENTY_FOUR_HOUR_TICKET_PREFIX))
            return create24HourTicketInstance(id);
        else if (id.startsWith(Constant.PREPAID_CARD_PREFIX))
            return createPrepaidCardInstance(id);
        else {
            System.err.println("ERROR: Travel permit code " + code + " is invalid.");
            return null;
        }
    }

    /**
     * @param ticketId the ID of ticket
     * @return the instance of class OneWayTicket
     * @throws SQLException
     */
    private OneWayTicket createOneWayTicketInstance(String ticketId) throws SQLException {
        float fare = OneWayTicket.getFareById(ticketId);
        OneWayTicket oneWayTicket = new OneWayTicket(ticketId, OneWayTicket.getBaseFareById(ticketId),
                TravelPermit.getLastActionById(ticketId), fare, OneWayTicket.getStatusById(ticketId),
                OneWayTicket.getRegisteredEmbarkationIdById(ticketId),
                OneWayTicket.getRegisteredDisembarkationIdById(ticketId), OneWayTicket.getEmbarkationIdById(ticketId));

        UserInterface.setTravelPermitType("One Way Ticket");
        UserInterface.setTravelPermitInfo("Ticket fare: " + fare + " " + Constant.CURRENCY);

        return oneWayTicket;
    }

    /**
     * @param ticketId the ID of ticket
     * @return the instance of class TwentyFourHourTicket
     * @throws SQLException
     */
    private TwentyFourHourTicket create24HourTicketInstance(String ticketId) throws SQLException {
        Timestamp firstUsedTime = TwentyFourHourTicket.getFirstUsedTimeById(ticketId);

        TwentyFourHourTicket twentyFourHourTicket = new TwentyFourHourTicket(ticketId,
                TravelPermit.getBaseFareById(ticketId), TravelPermit.getLastActionById(ticketId), firstUsedTime);

        UserInterface.setTravelPermitType("24-hour Ticket");
        if (firstUsedTime != null)
            UserInterface.setTravelPermitInfo("Valid until " + new Date(firstUsedTime.getTime() + 24 * 3600 * 1000));
        else
            UserInterface.setTravelPermitInfo(
                    "Valid until " + new Date(new Timestamp(System.currentTimeMillis()).getTime() + 24 * 3600 * 1000));

        return twentyFourHourTicket;
    }

    /**
     * @param cardId the ID of card
     * @return the instance of class PrepaidCard
     * @throws SQLException
     */
    private PrepaidCard createPrepaidCardInstance(String cardId) throws SQLException {
        float balance = PrepaidCard.getBalanceById(cardId);

        PrepaidCard prepaidCard = new PrepaidCard(cardId, TravelPermit.getBaseFareById(cardId),
                TravelPermit.getLastActionById(cardId), balance, PrepaidCard.getEmbarkationIdById(cardId));

        UserInterface.setTravelPermitType("Prepaid Card");
        UserInterface.setTravelPermitInfo("Balance: " + balance + " " + Constant.CURRENCY);

        return prepaidCard;
    }

    /**
     * @param barcode       the barcode string
     * @param embarkationId the ID of embarkation station
     * @return true if the passenger can enter, false otherwise
     * @throws SQLException
     * @throws InvalidIDException
     */
    public boolean enterStation(String barcode, int embarkationId) throws SQLException, InvalidIDException {
        TravelPermit travelPermit = createTravelPermitInstance(scanBarcode(barcode));

        UserInterface.showTravelPermitInfo();

        boolean response = travelPermit.enterStation(embarkationId);

        if (response)
            GateControlInterface.openGate();
        else
            GateControlInterface.closeGate();

        return response;
    }

    /**
     * @param barcode          the barcode string
     * @param disembarkationId the ID of disembarkation station
     * @return true if the passenger can enter, false otherwise
     * @throws SQLException
     * @throws InvalidIDException
     */
    public boolean exitStation(String barcode, int disembarkationId) throws SQLException, InvalidIDException {
        TravelPermit travelPermit = createTravelPermitInstance(scanBarcode(barcode));

        UserInterface.showTravelPermitInfo();

        boolean response = travelPermit.exitStation(disembarkationId);

        if (response)
            GateControlInterface.openGate();
        else
            GateControlInterface.closeGate();

        return response;
    }

}
