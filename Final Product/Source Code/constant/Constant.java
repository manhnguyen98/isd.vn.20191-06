package constant;

public class Constant {

	public static String DB_CONNECTION = "mysql";		
			
	public static String DB_HOST = "localhost";
	
	public static String DB_PORT = "3306";
	
	public static String DB_DATABASE = "AFC";
	
	public static String DB_USERNAME = "root";
	
	public static String DB_PASSWORD = "123456";

	public static int PREFIX_LENGTH = 2;
	
	public static String ONE_WAY_TICKET_PREFIX = "OW";
	
	public static String TWENTY_FOUR_HOUR_TICKET_PREFIX = "TF";
	
	public static String PREPAID_CARD_PREFIX = "PC";
	
	public static int BASE_DISTANCE = 5;
	
	public static int ADDITIONAL_DISTANCE = 2;
	
	public static float BASE_FARE = 1.9f;
	
	public static float ADDITIONAL_FARE = 0.4f;
	
	public static String CURRENCY = "euros";

}
