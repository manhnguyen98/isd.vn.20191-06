# Homework 3 - Usecase Specification
#### GROUP 6

## Description
Automated fare collection (AFC) system
- Clean repository
- Review the team's bitbucket repo fork process
  + Finish requirement analysis for the Automated Fare Controller (AFC) system
  + Update last week homework (use case) based on comments and discussion on class
  + Glossary: Define some common terminologies in the system
  + Do the specification for all use case (equally divided among the members)
  + Complete the SRS following the template provided by teacher

## Members' tasks
1. Nguyen Dich Long
- Update last week homework (use case) based on comments and discussion on class
- Do specification and activity diagram for use cases:
  + Enter platform area
- Aggregate members' assignments and complete the SRS

2. Vu Khanh Ly
- Do specification and activity diagram for use cases:
  + Scan prepaid card

3. Nguyen Huu Manh
- Do specification and activity diagram for use cases:
  + Exit platform area

## Review
| Doer | Reviewer | Status (%) |
| --- | --- | --- |
| Nguyen Dich Long | Nguyen Huu Manh | 100 |
| Vu Khanh Ly | Nguyen Dich Long | 100 |
| Nguyen Huu Manh | Nguyen Dich Long | 100 |
