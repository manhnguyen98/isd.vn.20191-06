# Homework 1.5 - Usecase Diagram - Part 2
#### GROUP 6

## Description
Automated fare collection (AFC) system
- Design the use case diagram using Astah
- Design flow of events for use cases

## Members' tasks
1. Nguyen Dich Long
- Design AFC use case diagram
- Design flow of events for use cases:
  + Enter platform area
  + Check one-way ticket's validity
  + Check 24h ticket's validity
  + Check prepaid card's validity
  + Check difference of one-way ticket's fare and travelling fare
  + Check difference of prepaid card's balance and travelling fare

2. Nguyen Huu Manh
- Design flow of events for use cases:
  + Exit platform area

## Review
| Doer | Reviewer | Status (%) |
| --- | --- | --- |
| Nguyen Dich Long | Nguyen Huu Manh | 100 |
| Nguyen Huu Manh | Nguyen Dich Long | 100 |
