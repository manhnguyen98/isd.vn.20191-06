# Homework 4 - Sequence Diagram
#### GROUP 6

## Description
Automated fare collection (AFC) system
- Create ArchitectureDesign folder
- Do the sequence diagram for all use case in AFC (equally divided among the members)


## Members' tasks
1. Nguyen Dich Long
- Do sequence diagram for use cases:
  + Enter platform area

2. Vu Khanh Ly
- Do sequence diagram for use cases:
  + Scan prepaid card
  + Recognize one-way ticket
  + Recognize 24-hour ticket

3. Nguyen Huu Manh
- Do sequence diagram for use cases:
  + Exit platform area

## Review
| Doer | Reviewer | Status (%) |
| --- | --- | --- |
| Nguyen Dich Long | Vu Khanh Ly | 100 |
| Vu Khanh Ly | Nguyen Huu Manh | 100 |
| Nguyen Huu Manh | Nguyen Dich Long | 100 |
