# Homework 7 - Database Design
#### GROUP 6

## Description
Automated fare collection (AFC) system
- Implement data modeling

## Members' tasks
1. Nguyen Dich Long
- Design E-R Diagram

2. Vu Khanh Ly
- Write documentation

3. Nguyen Huu Manh
- Convert E-R Diagram to Relational Model

## Review
| Doer | Reviewer | Status (%) |
| --- | --- | --- |
| Nguyen Dich Long | Vu Khanh Ly | 100 |
| Vu Khanh Ly | Nguyen Huu Manh | 100 |
| Nguyen Huu Manh | Nguyen Dich Long | 100 |

