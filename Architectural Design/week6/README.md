# Homework 5 - Communication Diagram and Class Diagram
#### GROUP 6

## Description
Automated fare collection (AFC) system
- Update sequence diagrams based on comments and discussion on class
- Do communication diagram and class diagram for all use case in AFC (equally divided among the members)
- Do combined class diagram for the whole system

## Members' tasks
1. Nguyen Dich Long
- Update sequence diagrams based on comments and discussion on class
- Do communication diagram and class diagram for use cases:
  + Enter platform area with prepaid card
  + Exit platform area with prepaid card
- Do combined class diagram for the whole system

2. Vu Khanh Ly
- Do communication diagram and class diagram for use cases:
  + Enter platform area with one-way ticket
  + Exit platform area with one-way ticket

3. Nguyen Huu Manh
- Do communication diagram and class diagram for use cases:
  + Enter platform area with 24-hour ticket
  + Exit platform area with 24-hour ticket

## Review
| Doer | Reviewer | Status (%) |
| --- | --- | --- |
| Nguyen Dich Long | Vu Khanh Ly | 100 |
| Vu Khanh Ly | Nguyen Huu Manh | 100 |
| Nguyen Huu Manh | Nguyen Dich Long | 100 |

